'''
    Created at 9/15/2020
    Author: Yuan-Chi Yang
    Objective: This script is used to extract the performance measures of the topic models, specifically
               the estimated perplexity based on a held-out corpus, 
               and the perplexity and coherence calculated at the end of each epoch
               using callbacks

'''


import pandas as pd
import re
import shutil

if __name__ == "__main__":
    logfile = 'model_callbacks.log' # the system's logging file for the callbacks from the LDA training
    f = open(logfile,'r')
    text = f.read()
    # obtraining the directory name for the output folder
    pattern = r'INFO:the storing dirname = (\S+)\n'
    dirname_regex = re.compile(pattern)
    temp = dirname_regex.findall(text)
    dirname = temp[0]

    # the perplexity estimate every "eval_every"
    pattern = r'INFO:([\d\.-]+) per-word bound, ([\d\.-]+) perplexity estimate based on a held-out corpus'
    perp_holdout_regex = re.compile(pattern) 
    perp_holdout = perp_holdout_regex.findall(text)

    # the perplexity estimate after every epoch
    pattern = r'INFO:Epoch (\d+): Perplexity estimate: ([\d\.-]+)'
    perp_epoch_regex = re.compile(pattern)
    perp_epoch = perp_epoch_regex.findall(text)


    # the coherence estimate after every epoch
    pattern = r'INFO:Epoch (\d+): Coherence estimate: ([\d\.-]+)'
    coh_epoch_regex = re.compile(pattern)
    coh_epoch = coh_epoch_regex.findall(text)

    df_perp_holdout = pd.DataFrame([{'per-word bound': x[0], 'perplexity':x[1]} for x in perp_holdout])
    df_perp_epoch = pd.DataFrame([{'epoch': x[0], 'perplexity':x[1]} for x in perp_epoch])
    df_coh_epoch = pd.DataFrame([{'epoch': x[0], 'coh':x[1]} for x in coh_epoch])

    df_perp_holdout.to_csv(f'{dirname}/df_perp_holdout.csv',index=False)
    df_perp_epoch.to_csv(f'{dirname}/df_perp_epoch.csv',index=False)
    df_coh_epoch.to_csv(f'{dirname}/df_coh_epoch.csv',index=False)

    f.close()
    shutil.move(logfile, f'{dirname}/{logfile}_back')

    print('extraction completed!')


