'''
    Data: Oct, 2020
    Author: Yuan-Chi Yang
    Objective: 
        This script train the LDA topic models
    Reference: 
        this script partially follows https://www.machinelearningplus.com/nlp/topic-modeling-gensim-python/
'''



import gensim
from gensim.models.wrappers import LdaMallet
from gensim.models.callbacks import PerplexityMetric,CoherenceMetric
import pandas as pd
import time
import os
import shutil
import json
from gensim.models import CoherenceModel
import logging, sys
import gc

# the gensim LDA export callbacks in the log file. The logging needs to be defined if one desires to investigate the callbacks
# the system's logging for LDA training
logging.basicConfig(filename='model_callbacks.log',#stream = sys.stdout,
                    format = "%(asctime)s:%(levelname)s:%(message)s",
                    level=logging.NOTSET)

def bow2weight(texts_id_bow, tweet_ids, ldamodel):
    '''
        This function express each tweet as a "bag of topic", i.e. it converts each tweet to \theta_d
        Params:
            texts_id_bow: the list of texts that are represeted as bag-of-words, using token id
            tweet_ids: the list of tweet id
            ldamodel: the ldemodel to be used
        Return:
            df_texts_topic: the dataframe with column ['tweet_id', \theta_{d,1},...,\theta_{d,K}]
    '''

    texts_topic = ldamodel[texts_id_bow]
    data_list = []
    for text_topic in texts_topic:
        data_dict = dict()
        for i in range(0,len(text_topic)):
            topic_i = text_topic[i][0]
            data_dict[f'topic_{topic_i}'] = text_topic[i][1]
        data_list.append(data_dict)
    df_texts_topic = pd.DataFrame(data_list)
    df_texts_topic['tweet_id'] = tweet_ids
    
    return df_texts_topic

def list2tuple(texts_id_bow):
    '''
        This function converts the list of list (as read from json file) to list of tuple (as required for gensim's LDA model)
        Params:
            texts_id_bow: the texts_id_bow as read from json file
        Return:
            the texts_id_bow after conversion
    '''
    return [(x[0],x[1]) for x in texts_id_bow]

def print_both(to_print,file):
    '''
        This function print the expression both to the screen and to a file (log file)
        Params:
            to_print: the string to be printed
            file: the path to the log file
        Return:
            None
    '''
    print(to_print)
    print(to_print, file=file)
	

if __name__ == "__main__":
    start_time = time.localtime()
    # The folder where the data is
    data_folder = '/home/yyang60/medicaid-political/data/20201010_131253-bow'
    
    # set-up the output folder
    work_folder = '/home/yyang60/medicaid-political'
    current_time_file = time.strftime("%Y%m%d_%H%M%S",time.localtime())
    dir_name = f'{work_folder}/results/{current_time_file}_gensim'
    if not os.path.exists(dir_name):
        os.mkdir(dir_name)
    # export the output folder to the logging
    logging.info(f'the storing dirname = {dir_name}')
    # the log file. This is our logging file, not the system's logging for LDA training.
    logfile = open(f'{dir_name}/logfile','w')
    
    print_both(f'Reading Data!\n', file=logfile)
    
    # read the training data
    train_dict = []
    with open(f'{data_folder}/train_dict.json','r') as f:
        for line in f.readlines():
            train_dict.append(json.loads(line))
    df_train = pd.DataFrame(train_dict)
    
    # read the validation data
    val_dict = []
    with open(f'{data_folder}/val_dict.json','r') as f:
        for line in f.readlines():
            val_dict.append(json.loads(line))
    df_val = pd.DataFrame(val_dict)
    id2token = gensim.utils.SaveLoad.load(f'{data_folder}/id2token')
    
    # define texts_id_bow, texts_lemmatized, and tweet_id for the training data
    texts_id_bow = [list2tuple(text_id_bow) for text_id_bow in df_train.text_id_bow.to_list()]
    texts_lemmatized = df_train.text_lemmatized.to_list()
    tweet_ids = df_train.tweet_id.to_list()
    
    # define texts_id_bow, texts_lemmatized, and tweet_id for the validation data
    texts_id_bow_val = [list2tuple(text_id_bow) for text_id_bow in df_val.text_id_bow.to_list()]
    texts_lemmatized_val = df_val.text_lemmatized.to_list()
    tweet_ids_val = df_val.tweet_id.to_list()
    
    # define the hyper-parameters to be tested
    num_topics_list = [13]#[5,10,13,15,17,20,25]
    alpha_list = ['auto']#,'symmetric', 0.8,0.5,0.3]
    
    # the list of the results
    data_list = []
    
    for num_topics in num_topics_list:
        for alpha in alpha_list:
            eta = None #'auto' # eta is beta in the lecture. A different naming system.
            print_both(f'For num_topics = {num_topics}, alpha = {alpha}, eta = {eta}',file=logfile)
            logging.debug(f'For num_topics = {num_topics}, alpha = {alpha}, eta = {eta}')
            
            ts = time.time()
            # define the perplexity and coherence callbacks
            perplexity_logger = PerplexityMetric(corpus=texts_id_bow, logger='shell')
            coherence_cv_logger = CoherenceMetric(corpus=texts_id_bow, logger='shell', coherence = 'c_v', texts = texts_lemmatized)
            # training gensim LDA model
            ldamodel = gensim.models.ldamodel.LdaModel(corpus=texts_id_bow, num_topics=num_topics, id2word=id2token, random_state = 123, update_every=10, alpha = alpha, eta = eta, passes = 100, callbacks=[perplexity_logger, coherence_cv_logger])
            te = time.time()
            print_both(f'It takes {te-ts} s to train the model for num_topics = {num_topics}, alpha = {alpha}, eta = {eta}!\n', file=logfile)
    
            logging.debug('End of model training')

            # save the model
            #ldamodel.save(f'{dir_name}/ldamodel_{num_topics}')
    
            # infer the weight for each tweet, df_train
            '''
            ts = time.time()
            df_train_texts_topic = bow2weight(texts_id_bow, tweet_ids, ldamodel)
            te = time.time()
            print_both(f'\tFor training data, it takes {te-ts} s to infer the weights!\n', file=logfile)
            
            df_train_texts_topic.to_csv(f'{dir_name}/df_train_texts_topic_{num_topics}.csv',index=False)
            '''
            # calculate the perplexity on the training data
            ts = time.time()
            perplexity_train = ldamodel.log_perplexity(texts_id_bow)
            print_both(f'\tFor training corpus, the perplexity is {perplexity_train}\n', file=logfile)
            te = time.time()
            print_both(f'\tit takes {te-ts} s to infer the perplexity!\n', file=logfile)
            # calculate the coherence on the training data with 10 top words
            ts = time.time()
            coherence_model_train = CoherenceModel(model=ldamodel, texts=texts_lemmatized, dictionary=id2token, coherence='c_v',topn=10)
            coherence_train = coherence_model_train.get_coherence()
            print_both(f'\tFor training corpus, the coherence is {coherence_train} for topn=10\n', file=logfile)
            te = time.time()
            print_both(f'\tit takes {te-ts} s to infer the coherence!\n', file=logfile)
            # calculate the coherence on the training data with 20 top words
            ts = time.time()
            coherence_model_train = CoherenceModel(model=ldamodel, texts=texts_lemmatized, dictionary=id2token, coherence='c_v',topn=20)
            coherence_train = coherence_model_train.get_coherence()
            print_both(f'\tFor training corpus, the coherence is {coherence_train} for topn=20\n', file=logfile)
            te = time.time()
            print_both(f'\tit takes {te-ts} s to infer the coherence!\n', file=logfile)
    
            # infer the weight for each tweet, df_val
            '''
            ts = time.time()
            df_val_texts_topic = bow2weight(texts_id_bow_val, tweet_ids_val, ldamodel)
            te = time.time()
            print_both(f'For validation, it takes {te-ts} s to infer the weights!\n', file=logfile)
        
            df_val_texts_topic.to_csv(f'{dir_name}/df_val_texts_topic.csv',index=False)
            '''
            # calculate the perplexity on the validation data
            ts = time.time()
            perplexity_val = ldamodel.log_perplexity(texts_id_bow_val)
            print_both(f'\tFor validation corpus, the perplexity is {perplexity_val}\n', file=logfile)
            te = time.time()
            print_both(f'\tit takes {te-ts} s to infer the perplexity!\n', file=logfile)
            # calculate the coherence on the validation data with 10 top words
            ts = time.time()
            coherence_model_val = CoherenceModel(model=ldamodel, texts=texts_lemmatized_val, dictionary=id2token, coherence='c_v',topn=10)
            coherence_val = coherence_model_val.get_coherence()
            print_both(f'\tFor validation corpus, the coherence is {coherence_val} for topn=10\n', file=logfile)
            te = time.time()
            print_both(f'\tit takes {te-ts} s to infer the coherence!\n', file=logfile)
            # calculate the coherence on the validation data with 20 top words
            ts = time.time()
            coherence_model_val = CoherenceModel(model=ldamodel, texts=texts_lemmatized_val, dictionary=id2token, coherence='c_v',topn=20)
            coherence_val = coherence_model_val.get_coherence()
            print_both(f'\tFor validation corpus, the coherence is {coherence_val} for topn=20\n', file=logfile)
            te = time.time()
            print_both(f'\tit takes {te-ts} s to infer the coherence!\n', file=logfile)
   
            # storing data
            data_dict = dict()
            data_dict['num_topics']=num_topics
            data_dict['alpha']=alpha
            data_dict['perplexity_train'] = perplexity_train
            data_dict['coherence_train'] = coherence_train
            data_dict['perplexity_val'] = perplexity_val
            data_dict['coherence_val'] = coherence_val
            data_list.append(data_dict)
            
            _ = gc.collect()
    
    # the dataframe of the performance of the LDA model using different hyper-parameters
    df_performance = pd.DataFrame(data_list)
    df_performance.to_csv(f'{dir_name}/df_performance.csv',index=False)
    
    shutil.copy(f'{work_folder}/topic_model_gensim.py',dir_name)
    # the pbs file for running on BMI cluster
    shutil.copy(f'{work_folder}/cluster_gensim.pbs',dir_name)
    # the shell file for running on BMI cluster
    shutil.copy(f'{work_folder}/run_topic_model_gensim.sh',dir_name)
    
    end_time = time.localtime()
    print('Script Running Summary:')
    print_both(f'\tstart at: {time.strftime("%c %Z",start_time)}', file=logfile)
    print_both(f'\tend at: {time.strftime("%c %Z",end_time)}', file=logfile)
    print_both(f'\tIt takes {time.mktime(end_time) - time.mktime(start_time)} s\n', file=logfile)

    print('Finished!!',file=logfile)
    logfile.close()
    print('Finished!!')      



